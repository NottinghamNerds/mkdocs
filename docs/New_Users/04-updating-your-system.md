# Updating Your System

In this section, we will discover how to update our system via the
built-in App Store, Pop!\_Shop! Something really cool and useful to note
here; most applications (including third-party applications) will be
updated along with your system when you complete your system updates!
Very rarely will you ever need to go out and download/update a piece of
software on it’s own outside of Pop!\_Shop! Typical operating systems
only update the system software and leave behind the other pieces of
software that their users have installed. Not here, not anymore!

So, we will just open Pop!\_Shop from our applications menu - this can
vary if you have done any customizations to your system. The easiest way
is to press your SUPER key (most people know this as the Windows key),
and type “shop”.

[![image-1618317420009.png](https://user-content.gitlab-static.net/2bf922479a608f4ce3a7a5ceb9dc95cc9784ea68/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f557949416853576d714343774477754e2d696d6167652d313631383331373432303030392e706e67)](https://user-content.gitlab-static.net/2bf922479a608f4ce3a7a5ceb9dc95cc9784ea68/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f557949416853576d714343774477754e2d696d6167652d313631383331373432303030392e706e67)

Once we have opened the Pop!\_Shop, at the top-middle of the window,
click the “Installed” tab.

[![image-1618317450314.png](https://user-content.gitlab-static.net/efd55bf72f26af467d0060eb1c0f08879939a36b/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f3339626a343435554338563733666b552d696d6167652d313631383331373435303331342e706e67)](https://user-content.gitlab-static.net/efd55bf72f26af467d0060eb1c0f08879939a36b/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f3339626a343435554338563733666b552d696d6167652d313631383331373435303331342e706e67)

Finally, at the top right of the window, click the “Update All” button.
This may ask for an administrator password in order to continue for
security purposes - the only person updating or adding software to a
system should be it’s owner or administrator!

[![image-1618317559033.png](https://user-content.gitlab-static.net/13e6a06d6be2d94ee7344572564fa60edae37df1/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f435654656a7158584f797338577073362d696d6167652d313631383331373535393033332e706e67)](https://user-content.gitlab-static.net/13e6a06d6be2d94ee7344572564fa60edae37df1/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f435654656a7158584f797338577073362d696d6167652d313631383331373535393033332e706e67)

And that’s it! You’ve updated your system! See you in the next article.

If you want to make any customizations to your system like moving your
task bar, adding a “Start Menu”, or adding plugins to your desktop,
check out the next section: [Make it just
right](https://docs.nottinghamnerds.com/books/new-users/page/make-it-just-right "Make it just right")
