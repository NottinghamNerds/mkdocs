# What is Open Source & Linux?

The Free and Open Source Software (FOSS) model of developing software is
vital in ensuring that the software and technology we use every day
remains usable, relevant, and open. The software development and
computer science industry as a whole is booming, but many companies are
still building software in-house with lean development teams and
maintaining closed-source software (CSS), also known as proprietary
software. This is an extraordinarily inferior way to manage and develop
software when compared to open-source software. Licensing software as
open-source nets both businesses and software users a plethora of
positive outcomes, but is often over-looked. Free and Open Source
Software is superior to Closed-Source Software because open-source
software is much more collaborative, secure, integrable, and scalable
while allowing its users to completely own their software.

The vast amount of collaboration that can occur on a single open-source
software project is staggering. Software developers from all around the
world working together to achieve a single goal is amazing in and of
itself, and the pure amount of innovation that occurs when the minds of
so many experts are united by a shared passion for a project is
absolutely mind-blowing. Working on a FOSS project allows anyone from
anywhere on the planet with the necessary skills to do so and with the
passion about the project to have the power to work on that project.
Even better, anyone that does not necessarily have the skills to write
software can help, too! Non-developer community members can assist
projects by writing supporting documentation, designing a project
website, submitting bug reports, providing information about using the
software in edge-cases of all kinds, donating money to support the
developers, or even just contributing ideas that would push the project
forward. Another very interesting fact is that the open-source paradigm
is not useful solely in the software development space, but in other
areas of expertise as well. The science community has been rapidly
moving over to open source software and has begun to adopt the idea of
open literature so that scientific discoveries can not only be made
public, but can be worked on by anyone that wishes to do. This also
prevents keeping these discoveries behind closed doors or some type of
pay-wall and away from the general public. Interestingly, the adoption
of open-source software is becoming more widely accepted in science,
pushing the limits of science further and further, to previously
unimaginable horizons.

Open-source software is undeniably more secure than closed-source
software. The possibility of malfeasance with regards to source code in
open-source is almost none since thousands of pairs of eyes are digging
through the source on any given day, assuming the project is well known
by the community. Being that open-source code is viewable and modifiable
by anyone, and that the open source community is jam-packed with
incredibly intelligent software developers, IT administrators, and
technologists of all kinds, many of <span lang="en-US">them</span>
understand how to write secure software and are able to implement their
code in ways that reduce the risk of vulnerabilities. Even in cases
where the code has a vulnerability, it can be quickly found and patched
not only due to the absurd amount of <span lang="en-US">people</span>
that are working on these projects, but security experts can audit the
code day or night from all over the globe and let the developers of the
project know of any issues so they can quickly and decisively fix them.
Since the development teams are often scattered across multiple
countries and continents, the open-source community never sleeps, and
their reaction times are often much faster when compared to a business
updating its proprietary software. Even better, the results that
security experts come to are verifiable because they can release their
testing methods so that others can test the software to ensure they find
the same results. When compared to Linux, other (proprietary) operating
systems like Windows and Mac are subject to lots of viruses, since
audits cannot be done openly.

Software developers and businesses alike that embrace the open-source
model also net something that is phenomenally advantageous: scalability.
The open-source model fosters the ability for software to be scaled in
ways that closed-software could never accomplish. Firstly, it allows a
project to scale with community interest – that is, the more useful the
project is to its users, the more people discover it and inevitably the
more developers and community members support that project. FOSS also,
in some cases, allows for software services to be federated. For
projects that are server/client based, federated services allow
community members and users to create their own servers in which other
users can connect to with the client; this is favorable because if the
project itself ceases to operate an instance of the server, community
members can maintain use of the software by hosting servers themselves;
an added bonus to this is that no single company can shut down the
service by turning their servers off and leaving customers and users out
in the cold. Another great thing about this is a user may not even have
the necessary hardware to accomplish this, so they can upload the source
code/binaries to a Virtual Private Server (VPS) on infrastructure owned
by a cloud provider, and run the software there. Lastly, users can often
self-host software in their own homes so that all of the data associated
with the software is on their own hardware, and their personal data is
not being vacuumed up by data-mining companies like Facebook, Google,
and Amazon in order to sell both the data and ads. <span
lang="en-US">T</span>he Free Software Foundation has been encouraging
software companies and software service providers to license their
software as open-source so that users have a say in what happens with
the data collected during use of such software - and as we all know,
data security and privacy are massive issues since the Snowden leaks in
2013.

Open-source software has even more benefits: ownership and integration.
It is relatively rare that you will find open-source software that does
not implement open protocols. Open protocols allow the software
community to design protocols for interfacing with hardware devices and
software applications in a way which is mutually agreed upon rather than
utilizing proprietary standards which only apply when developers wish to
interface with a proprietary end-point. Utilizing open protocols allows
developers to ensure their applications are, figuratively speaking, the
same language in order to work together. Open-source doesn’t stop there,
though. Developers often support multiple proprietary protocols on top
of supporting open protocols in order to integrate with as many
(relevant) formats as possible so that their users don’t suffer from
vendor lock-in. A great example is how Microsoft’s operating system
monopoly, for instance, is largely predicated on the abundance of
software that is only available on Windows. Proprietary software vendors
often attempt to lock their users into their platform by creating their
own versions of well-known protocols/file formats or otherwise
close-sourcing their software so that once a user has bought into one
piece of software, they need to buy other software from the same vendor
or from vendors who support that vendor to ensure everything works;
these types of things are common-ground for companies like Microsoft
(Windows, Office), Adobe (entire software suite), and Apple (both
hardware and software) where migration to or from these solutions is
made next to impossible. Open-source also supports user ownership – FOSS
is often associated with freedom, hence the free in Free and Open-Source
Software. Freedom to view, audit, modify, and redistribute software;
freedom to choose where your data is held (and whether it is sold or
kept private). While FOSS is often also free (doesn’t cost money),
open-source developers typically build these software solutions in their
free time and don’t charge their users for use, so it is encouraged that
monetary support is given to open-source projects. Although there are
many open-source developers that do so as full-time employees, such as
at companies like GitLab, Redhat, and System76, they all need help.

It is often argued by businesses who write proprietary software that
software scalability with regards to cloud infrastructure is pushed
almost entirely by large corporations who themselves both support and
make their money from creating proprietary software. This is an
undeniable fact with regards to hardware, however, that while those
businesses often have the most money (because they sell their
closed-source software rather than give it away so that mankind may
thrust itself forward rather than stall innovation due to things such as
money), there is a large hole in this logic. Large businesses such as
those aforementioned, generally speaking, own much more hardware
infrastructure because they have the ability to purchase massive data
centers. In this regard, this argument holds water, however, every
single one of those data centers (owned by all of the largest cloud
providers; Amazon, Google, and even Microsoft) run the Linux operating
system. Linux is – you guessed it – open source software. The cloud as
we know it would not function the same or be nearly as fast if Linux
wasn’t powering the infrastructure that most people use unknowingly
every single day. From web services, to websites, to smart devices, to
gaming servers, music streaming, and video streaming, Linux is behind it
all. Another common argument made by proprietary software lovers is with
regards to collaboration. Namely that collaboration only occurs in
open-source when the project is discovered by the community and that
while collaboration in a proprietary setting is often forced by the
company, the team marches toward a single vision. While this is true,
not all software should live on – only the best, most useful software
should be pushed forward. It is incredibly rare that a well-known,
incredibly useful FOSS project goes unnoticed in the open-source
community and dies off as a result. By its very nature, open-source
software is capable of easily being discovered because it is open, and
often easily found on websites that serve as the version control systems
for such software such as GitLab, GitHub, and BitBucket, whether the
software is already popular or not. In addition, one of the most
powerful things about Linux and other open source software is the
freedom of choice. <span lang="en-US">O</span>pen-source is an amazing
alternative to closed-source because of the sheer amount of options.
Proprietary software often yields one or two choices for users to choose
between and they are not often very great whereas in open source, while
sometimes it may seem like fragmentation, FOSS always has multiple
options so the users can pick and choose which piece of software suits
their needs perfectly.

Businesses who are already building proprietary software may hesitate to
embrace the open-source model because they believe they cannot make
money doing so. This is an outrageously incorrect way of thinking;
companies like Redhat and Arm Ltd are open-source all the way down to
their cores, and in the last couple of years, both sold for ~$34 billion
and ~$40 billion respectively. Redhat, for example, makes its money by
selling support and managed services to its customers while allowing
their customers to use their software free-of-charge. This is
advantageous because not only can anyone utilize the software, people
can learn and contribute to the software without barriers. In addition,
users can create web portals/communities to share content, code, ideas,
and use cases which in the end work to the advantage of Redhat because
their community begins to build and manage itself as well as build and
manage things that a closed-source based company would otherwise have to
do itself. Lastly, it a well-known and often shared idea about
open-source is that since its users can use the software for free,
up-and-coming technologists, software developers, IT personnel, and
students can utilize the software in order to better their lives without
fear of having to pay a figurative arm and leg for a license in order to
do so. This allows anyone from anywhere to create, collaborate, secure,
scale, and integrate the software technologies around them instead of
being stunted because they couldn’t afford a license for a piece of
proprietary software.
