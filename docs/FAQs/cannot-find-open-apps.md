# I Can't Find My Apps!

Pop!\_OS is a really great Operating System based on the Gnome Desktop
environment. Sometimes, because of some of the advanced features of
Gnome (like work spaces a.k.a. virtual desktops), we can sometimes
misplace are open applications by accidentally moving to a different
work space. Luckily, this is an easy fix and may allow some to start
exploring some of the work flows that Gnome wishes to bring to its
users. Work spaces can be a very powerful way to group applications and
create work flows so that everything you are working on can be placed
where you want them, with other similar applications.

[![image-1618331760378.png](https://user-content.gitlab-static.net/da1ed706b8db1b8c76f91597ef35c65826a997e5/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f7a46686d7744377a5237373236384a532d696d6167652d313631383333313736303337382e706e67)](https://user-content.gitlab-static.net/da1ed706b8db1b8c76f91597ef35c65826a997e5/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f7a46686d7744377a5237373236384a532d696d6167652d313631383333313736303337382e706e67)

So we have lost our Firefox browser because we accidentally switch work
spaces. We see the line under the Firefox icon at the bottom left
signifying that the application is open, but we don’t see it on our
desktop. If you are using the Dash to Panel customization, you’ll see
something like the above - otherwise you may need to press your
SUPER/Windows key in order to get into the overview.

[![image-1618331854675.png](https://user-content.gitlab-static.net/146b89c66f221554753dd25d12ef0e464ff55327/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f477050563336686631756e556c62366d2d696d6167652d313631383333313835343637352e706e67)](https://user-content.gitlab-static.net/146b89c66f221554753dd25d12ef0e464ff55327/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f477050563336686631756e556c62366d2d696d6167652d313631383333313835343637352e706e67)

In a default installation, your applications would show up on the
left-hand side of the screen, rather than the bottom. On the right,
you’ll notice we have a blank desktop selected, and above it there seems
to be an open window. From here, you may either click the Firefox icon
from your launch panel, or you can click the work space with the Firefox
browser.

[![image-1618331984379.png](https://user-content.gitlab-static.net/2e13ab0e61c24cf17f48c178aa6108a47c7a3602/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f4d6f75457a78315173394d684a476b4b2d696d6167652d313631383333313938343337392e706e67)](https://user-content.gitlab-static.net/2e13ab0e61c24cf17f48c178aa6108a47c7a3602/68747470733a2f2f646f63732e6e6f7474696e6768616d6e657264732e636f6d2f75706c6f6164732f696d616765732f67616c6c6572792f323032312d30342f7363616c65642d313638302d2f4d6f75457a78315173394d684a476b4b2d696d6167652d313631383333313938343337392e706e67)

Once clicked, you’ll see the browser in the newly selected work space
(the default work space or work space number 1), and the Firefox browser
is shown in the overview menu. From here, we can either click the
application we wish to use (there can be multiple), or press either our
Esc key or SUPER/Windows key to come out of the Overview mode.

All set! If you have any burning questions, send an
email to support@nottinghamnerds.com!
