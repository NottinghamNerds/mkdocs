# Where Are My Minimize And Maximize Buttons?

They are hidden by default because the creator of the Gnome Desktop
Environment has a vision about how users should interact with the user
interface of their computer. That vision doesn’t include minimize or
maximize buttons. To fix this, checkout our [Make it just
right](https://nottinghamnerds.gitlab.io/mkdocs/New_Users/05-make-it-just-right "Make it just right")
section which explains some small customizations we recommend for our
customers. Most of the customizations we recommend utilize a tool called
Gnome Tweaks, and there are a couple of options within Gnome Tweaks that
allow us to turn on our minimize/maximize buttons.
